# In Vitro / In Vivo Controlled Release Correlation Tool
# Generalized Code for a single drug/formulation
# REORGANIZED FOR PORT INTO SPOTFIRE

# Written by Brett Geiger (MIT) on 10/27/16
# Last Modified by Brett Geiger on 06/29/17

# UNCOMMENT THE BELOW IF FIRST TIME RUNNING THIS CODE
#install.packages("deSolve")
#install.packages("MESS")
#install.packages("sensitivity")
#install.packages("PK")

library(deSolve) # Access deSolve package. MUST HAVE PACKAGE LOADED TO FUNCTION
# See deSolve R vignette online for more information (Google deSolve)
library(MESS) # Access package with Area Under Curve (AUC) integration methods
library(sensitivity) # Access package with sensitivity functions
library(PK) # Access package with PK functions (used for SSbiexp)
## Model Build
# First establish intervals of interest in time and space

# USER DATA ENTRY: ENTER DESIRED TIME OF SIMULATION
t0<-0 # USER DATA ENTRY: SIMULATION START TIME (hours)
# SET DEFAULT TO 0
tmax<-168 # USER DATA ENTRY: SIMULATION END TIME (hours)
# NO DEFAULT ALLOWED
tspace<-.1 # USER DATA ENTRY: SIMULATION TIME INCREMENT (hours)
# SET DEFAULT TO (TMAX-T0)/2000

d0<-5 # USER DATA ENTRY: SIMULATION START DISTANCE (mm).  Usually the radius of the formulation
# SET DEFAULT TO 0 BUT THIS WILL CAUSE MINOR INACCURACY 
dmax<-11 # USER DATA ENTRY: SIMULATION END DISTANCE (mm), (dmax = formulation radius + radius of interest)
# NO DEFAULT ALLOWED
dspace<-.03 # USER DATA ENTRY: SIMULATION DISTANCE INCREMENT (mm) NOTE: There must be substantially 
# more steps in time than in distance or you will see considerable sensitivity to dspace
# SET DEFAULT TO (DMAX-D0)/200

dstart<-d0+dspace # Indexing correction to d0, CODE USES THIS VALUE

# Establish flow, transport and clearance parameters from literature

# Here we have an intrinsic (unitless ~ 1/(time)) clearance rate, making the 
# system unit independent with respect to concentration.
kclear <- 1 # USER DATA ENTRY: (1/hr) FIRST ORDER CLEARANCE RATE
# NO DEFAULT ALLOWED
# I would recommend actually measuring this "first order clearance rate" in vivo
# Or somehow acquiring in house data from the PKPD or local delivery team
flowrate <- 1.67 # USER DATA ENTRY: (mm/hr) LINEAR CONVECTION RATE only used to incorporate convection
# SET DEFAULT TO 0, ASSUMING NO CONVECTION IN MODEL
diffusivity <- 3.312/2.5 # USER DATA ENTRY: (mm^2/hour) DRUG DIFFUSIVITY
# NO DEFAULT ALLOWED

# Establish formulation controlled release parameters from in vitro dissolution experiment

csource<- 3500 # USER DATA ENTRY: pmol/mm^3 (uM) DRUG CONCENTRATION
# WITHIN FORMULATION OR AT FORMULATION/TISSUE INTERFACE
# NO DEFAULT ALLOWED

# IMPORT EXCEL FILE FROM DISSOLUTION EXPERIMENT.  TAKES TWO COLUMN VECTORS OF DATA
# WITH A HEADER IN .CSV FORMAT
# 1: "t" (hours) which contains times, and 2: "fr" (unitless), such that 0<"fr"<1,
# which contains fractions of initial drug concentration released and measured

diss<-read.table("CarmData.csv", header = TRUE, sep = ",")
# CarmData.csv is an example table from the model validation

# Three functions are now defined, one for each possible fit method
# (exponential,linear,biexponential).  The function fits the data imported
# from the .csv file above, stores and returns a vector of fit parameters tau,
# and plots the fit over the original data.  A switch case statement at the end
# allows the user to select the appropriate fit

expfit<-function(diss)
{
  expC <- lm(log(diss$fr) ~ diss$t + 0) # This is a one phase exponential decay
  tau <- expC$coefficients # extract fit coefficients
  # tau <- 0 Uncomment to set drug loss to 0 and approximate steady state
  
  # Validate the fit by plotting, confirm that fit to in vitro data is reasonable
  
  Drug.model <- exp(predict(expC, as.list(diss$t)))
  plot(diss$t,1-diss$fr,pch = 16,main = "Dissolution data and fit")
  lines(diss$t, 1-Drug.model, lwd=2, col = "red", xlab = "Time (h)", ylab = "Drug")
  # USER OUTPUT - ALLOWS USER TO VISUALLY EVALUATE FIT
  return(tau)
}


#Linear fit
linfit<-function(diss){
  expC <- lm(diss$fr ~ diss$t) # This is linear fit
  tau <- expC$coefficients # indexed as [intercept,slope]
  # tau <- 0 Uncomment to set drug loss to 0 and approximate steady state
  
  Drug.model <- predict(expC, as.list(diss$t))
  plot(diss$t,1-diss$fr,pch = 16,main = "Dissolution data and fit")
  lines(diss$t, 1-Drug.model, lwd=2, col = "red", xlab = "Time (h)", ylab = "Drug")
  # USER OUTPUT - ALLOWS USER TO VISUALLY EVALUATE FIT
  return(tau)
}

#Biexponential fit
biexpfit<-function(diss){
  expC <- biexp(diss$fr,diss$t) # This is biexp decay  y = A1exp(-k1*t)+A2exp(-k2*t)
  # Use with caution.  Will fit better than "exp", but it is physically relevant?
  tau <- data.matrix(expC$parms)[2-3,] # indexed as [k1,A1,k2,A2]
  
  Drug.model <- SSbiexp(diss$t,tau[2],log(tau[1]),tau[4],log(tau[3]))
  plot(diss$t,1-diss$fr,pch = 16,main = "Dissolution data and fit")
  lines(diss$t, 1-Drug.model, lwd=2, col = "red", xlab = "Time (h)", ylab = "Drug")
  # USER OUTPUT - ALLOWS USER TO VISUALLY EVALUATE FIT
  return(tau)
}

# Determine the chosen fit method for input data
fit<-"lin" # USER DATA ENTRY: options are "exp" - exponential, "lin" - linear,
# "biexp" biexponential.

# A switch case statement defines our dissolution decay parameter vector tau
# based on the chosen fit
tau<-switch(fit,exp = expfit(diss), lin = linfit(diss), biexp = biexpfit(diss))

# BELOW IS THE ROUTINE IMPLEMENTED TO GENERATE OUTPUT

# Define a function containing the fundamental PDE
# dc/dt = -divergence(N) - k*c -v*divergence(c)where c(x,t) is concentration
# N is flux (N = -D*dc/dr where D = diffusivity), v is flowrate (a linear velocity)
# k is a clearance rate in 1/hour

# Currently in cylindrical coordinates - definition of grad operator changes
# will not affect ranking different systems, will affect depth of penetration into tissue.  
# MAY ALLOW SELECTION OF RECT, CYL, OR SPHER coordinates IN FUTURE

# Initialize key parameters for PDE function "release" to take  
rad <- seq(dstart,dmax+dspace,dspace) # storing radius for each step
idx <- length(rad) # Clean up indexing in function
times <- seq(t0,tmax, by = tspace) # set time over which to run function
cprof <- c(rep(0,idx)) # initialize concentration profile

# Combine key parameters into a single object that is passed to the function
params <- list(rstep = dspace, D = diffusivity, r = rad, k = kclear, c0 = csource, tau = tau, v = flowrate, fit = fit)

# PDE function release - this is a numerical approximation of the distance derivatives
# in the PDE using the Method of Lines/Boxes (the diff() function).  
# Then an ODE function can iterate it to solve the time derivative.
# Details in the deSolve vignette.  The diff() function makes indexing a bit challenging
# Warnings about objects not being the same length means indexing has gone awry

# Based on Fick's laws of diffusion with first order kinetic clearance

release <- function(times,cprof,params) {
  with(params,{ # Access the variables in params list object
    dx1 <- rep(rstep,idx+1) # Space between lines/boxes
    switch(fit, # The fit variable was passed to this function along with tau
           exp = cdev <- c0*exp(times*tau),
           lin = cdev <- c0*(tau[2]*times+tau[1]),
           biexp = cdev <- c0*(tau[2]*exp(-tau[1]*times)+tau[4]*exp(-tau[3]*times)))
    flux<- -D*diff(c(cdev,cprof,cprof[idx]))/dx1 # Addition of 0 has forced a 0 conc condition at max distance
    dx2 <- rep(rstep,idx)
    dcdt<- -(1/r)*(diff(c(r,r[idx]+rstep)*flux)/dx2)-k*cprof-v*(1/r)*(diff(c((r[1]-rstep),r)*c(cdev,cprof))/dx2)
    # This is convection from interstitial flow in the direction of diffusion.  Convective clearance looks different
    list(dcdt)
  })
}

# ODE function - iterates "release" over "times".  Tracks cprof, passes params.
# Use "help(ode.1D)" to answer questions
output<-ode.1D(cprof,times,release,params,nspec = 1, names = "Conc")

# # Heatmap display of release - NOT NECESSARY IN INITIAL VERSION
# image(output, method = "filled.contour", grid = rad,
#      xlab = "time (hours)", ylab = "distance (mm)",
#      main = "Concentration Profile (uM)")

# Line plot display of release.  Show different times by changing subset
timeset<- c(24,72,120,168)
# WANT TO IMPLEMENT SLIDERS IN SPOTFIRE TO TO ADJUST DISPLAYED TIME IN "timeset"
matplot.1D(output,grid = rad, type = "l",
           subset = times %in% timeset, main = "Drug release profile",
           xlab = "Distance (mm)", ylab = "Concentration (uM)", xlim = c(dstart,dmax), ylim = c(0,csource))
# MAIN OUTPUT OF MODEL


# # Sensitivity Analysis, perform with diffusivity, kclear, c0, and tau
# # Need a single quantifiable parameter - use area under the concentration-distance curve (AUC)
# # These next blocks will be local sensitivity analysis - measuring the change in AUC as a response
# # to adjusting one of the input parameters at a time (1st order effects).  Simple, useful, but sometimes the most powerful
# # effects involve interactions (2nd order effects) between two parameters like k/D
# calcauc <- function(timepoint,rad,DEout) { # Takes the time of interest, distance, and our output from the model DEout
#   curve<-DEout[timepoint,2:(length(rad)+1)]
#   auc<- auc(rad,curve,type="spline")}
# # Computes and outputs AUC
# # Redefine timeset by index instead of actual time
# timeset<-timeset/tspace
# 
# # Sensitivity analysis on diffusivity
# sensrange<-c(.001,.01,.1,1,10,100,1000) # Set physiologically reasonable range
# diffsens = matrix(nrow = 7, ncol = length(timeset));
# ind <- 0 # Establish an index counter to help store results
# for (i in sensrange) {
#   ind <- ind+1
#   diffusivitys<-i*diffusivity # Change value and run model function
#   params <- list(rstep = dspace, D = diffusivitys, r = rad, k = kclear, c0 = csource, tau = tau, v = flowrate)
#   output<-ode.1D(cprof,times,release,params,nspec = 1, names = "Conc")
#   for (j in timeset){
#     diffsens[ind,match(j,timeset)] <- calcauc(j,rad,output) # Store result for that value, here it is calculated at times in timeset  
#   }}
#   # Plot AUC at each value of parameter
# matplot(log10(sensrange),diffsens, main = "Sensitivity of AUC to Diffusivity", 
# xlab = "log10(Initial Diffusivity (D))", ylab = "AUC", type = "l", col = c("black","red","green","blue"))
# 
# # Sensitivity analysis on clearance
# sensrange<-c(.125,.25,.5,1,2,4,8) # Set physiologically reasonable range
# ksens = matrix(nrow = 7, ncol = length(timeset))
# ind <- 0 # Establish an index counter to help store variables
# for (i in sensrange) {
#   ind <- ind+1
#   ks<-i*kclear # Change value and run model function
#   params <- list(rstep = dspace, D = diffusivity, r = rad, k = ks, c0 = csource, tau = tau, v = flowrate)
#   output<-ode.1D(cprof,times,release,params,nspec = 1, names = "Conc")
#   for (j in timeset){
#     ksens[ind,match(j,timeset)] <- calcauc(j,rad,output) # Store result for that value, here it is calculated at times in timeset  
#   }
# }
# # Plot AUC at each value of parameter
# matplot(log2(sensrange),ksens, main = "Sensitivity of AUC to Clearance Constant",
#      xlab = "log2(Initial Clearance (k))", ylab = "AUC", type = "l", col = c("black","red","green","blue"))
# 
# # Sensitivity analysis on Csource
# sensrange<-c(.125,.25,.5,1,2,4,8) # Set physiologically reasonable range
# cosens = matrix(nrow = 7, ncol = length(timeset))
# ind <- 0 # Establish an index counter to help store variables
# for (i in sensrange) {
#   ind <- ind+1
#   cos<-i*csource # Change value and run model function
#   params <- list(rstep = dspace, D = diffusivity, r = rad, k = kclear, c0 = cos, tau = tau, v = flowrate)
#   output<-ode.1D(cprof,times,release,params,nspec = 1, names = "Conc")
#   for (j in timeset){
#     cosens[ind,match(j,timeset)] <- calcauc(j,rad,output) # Store result for that value, here it is calculated at times in timeset  
#   }
# }
# # Plot AUC at each value of parameter
# matplot(log2(sensrange),cosens, main = "Sensitivity of AUC to Initial Interface Concentration",
#      xlab = "log2(Initial Concentration)", ylab = "AUC", type = "l", col = c("black","red","green","blue"))
# 
# # Sensitivity analysis on tau
# 
# sensrange<-c(.125,.25,.5,1,2,4,8) # Set physiologically reasonable range
# tausens = matrix(nrow = 7, ncol = length(timeset))
# ind <- 0 # Establish an index counter to help store variables
# for (i in sensrange) {
#   ind <- ind+1
#   taus<-i*tau # Change value and run model function
#   params <- list(rstep = dspace, D = diffusivity, r = rad, k = kclear, c0 = csource, tau = taus, v = flowrate)
#   output<-ode.1D(cprof,times,release,params,nspec = 1, names = "Conc")
#   for (j in timeset){
#     tausens[ind,match(j,timeset)] <- calcauc(j,rad,output) # Store result for that value, here it is calculated at times in timeset  
#   }
# }
# # Plot AUC at each value of parameter
# matplot(log2(sensrange),tausens, main = "Sensitivity of AUC to Release Rate constant",
#      xlab = "log2(Tau)", ylab = "AUC", type = "l", col = c("black","red","green","blue"))

# # Global Analysis - This is much more rigorous and investigates interaction between the parameters
# # I am less comfortable with the math that is going on here - it has been difficult to code since
# # a rigorous global sensitivity analysis requires an enormous amount of runs of the function, which
# # takes a lot of computational time on my laptop.  However, this provides useful information about
# # which parameters (1: D, 2: k, 3: c0 4: tau) AND THEIR INTERACTIONS are most important
# n <- 1000 # Number of random samples - does not work well at <1000, but this number is what
# # drives computational cost
# X1 <- data.frame(matrix(runif(4*n, min = -3, max = 3),nrow = n))
# X2 <- data.frame(matrix(runif(4*n, min = -3, max = 3),nrow = n))
# # A matrix to randomize your 4 parameters on a uniform distribution from -8 to 8
# pset <- matrix(rep(c(diffusivity,kclear,csource,tau),n),nrow = n,byrow = TRUE)
# R1 <- data.frame(pset*2^X1)
# R2 <-data.frame(pset*2^X2)
# # Convert your starting values of parameters to the randomized matrix
# 
# # Define a function that takes an input matrix of n rows of a random uniform dist. of your 4 parameters
# # and produces a single output metric, in this case, AUC
# releasesens <- function(paramrange){
#   metric<-vector("numeric",nrow(paramrange)) # initialize output
#   for (j in 1:nrow(paramrange)){ # Iterate n times, this is why it takes a long time
#     params <- list(rstep = dspace, D = paramrange$X1[j], r = rad, k = paramrange$X2[j], c0 = paramrange$X3[j], tau = paramrange$X4[j], v = flowrate)
#     cprof <- c(rep(0,idx)) # initialize concentration profile
#     output<-ode.1D(cprof,times,release,params,nspec = 1, names = "Conc") # Run function
#     metric[j]<-calcauc(240,rad,output) # Store this iteration
#   } 
#   metric}
# 
# globalsens<-sobol(model = releasesens,X1=R1,X2=R2,order=2, nboot = 10, conf = .90)
# # Runs the sobol senstivity analysis, results are stored in globalsens
# plot(globalsens, main = "Sobol Global Sensitivity Indices and 90% CIs", xlab = "Variable", ylab = "Sobol Index", col = "dark red")
